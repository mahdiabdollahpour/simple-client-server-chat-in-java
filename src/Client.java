import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;


public class Client extends JFrame {
    private JTextField enterField;
    private JTextArea displayArea;
    private ObjectOutputStream output;
    private ObjectInputStream input;
    private String massege = "";
    private String chatServer;
    private Socket client;


    public Client(String host) {
        super("Client");
        chatServer = host;
        Container container = getContentPane();
        enterField = new JTextField();
        enterField.setEnabled(false);
        enterField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sendData(
                        e.getActionCommand()//gives the value of the text field
                );
            }
        });
        container.add(enterField, BorderLayout.NORTH);
        displayArea = new JTextArea();
        container.add(new JScrollPane(displayArea), BorderLayout.CENTER);
        setSize(300, 150);
        setVisible(true);
    }

    public void runClient() {
        try {
            connectToServer();
            getStreams();
            processConnection();
            closeConnection();


        } catch (
                EOFException eofException)

        {
            System.out.println("server terminated connection");
        } catch (
                IOException ioException)

        {
            ioException.printStackTrace();
        }

    }


    private void getStreams() throws IOException {
        output = new ObjectOutputStream(client.getOutputStream());
        output.flush();
        input = new ObjectInputStream(client.getInputStream());
        displayArea.append("\n got I/O stream\n");


    }

    private void connectToServer() throws IOException {
        displayArea.setText("attempting to connection\n");
        System.out.println("hi");
        client = new Socket(InetAddress.getByName(chatServer),5000);
        System.out.println("hi again");
        displayArea.append(" connected to : " + client.getInetAddress().getHostName());

    }

    private void processConnection() {

        enterField.setEnabled(true);
        do {
            try {
                massege = (String) input.readObject();
                displayArea.append("\n" + massege);
                displayArea.setCaretPosition(displayArea.getText().length());
            } catch (EOFException e) {
                displayArea.append("\n client terminated connection");
            } catch (ClassNotFoundException e) {
                displayArea.append("\n unkown object recieved");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } while (!massege.equals("server >>> terminate"));
    }

    private void closeConnection() throws IOException {
        displayArea.append("\n closing connetion");
        output.close();
        input.close();
        client.close();
    }

    private void sendData(String massege) {
        ;;;try {
            output.writeObject("client >>> " + massege);
            output.flush();
            displayArea.append("\nclient>>>" + massege);
        } catch (IOException e) {
            displayArea.append("\n error writing object");
        }
    }

    public static void main(String[] args) {
        Client application;
        if (args.length == 0) {
            application = new Client("127.0.0.1");
            //application = new Client("192.168.230.1");

        } else {
            application = new Client(args[0]);
        }
        application.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        application.runClient();
    }
}
