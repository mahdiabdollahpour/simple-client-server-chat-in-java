package mine;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;

/**
 * Created by msi on 5/29/2017.
 */
public class MyChatHandler extends Thread {
    public ObjectInputStream i;
    public ObjectOutputStream o;
    private JTextArea displayArea;
    private String name;
    public Socket mS;
    private boolean isConnected = true;

    public MyChatHandler(Socket s, JTextArea DisplayArea, String Name) {
        System.out.println("chat handeler starting");
        this.mS = s;
        this.displayArea = DisplayArea;
        this.name = Name;
        try {
            i = new ObjectInputStream(s.getInputStream());
            o = new ObjectOutputStream(s.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        Thread t = new Thread(this);
        t.start();
    }

    private static ArrayList<MyChatHandler> handlers = new ArrayList<>();

    public void run() {
        handlers.add(this);
        System.out.println("number of handlers : " + handlers.size());

        while (true) {
            try {

                String msg = (String) i.readObject();
                if (msg.equals("terminate")) {
                    //   if (isConnected) {
                    handlers.remove(this);
                    displayArea.append("\n" + name + " terminated connenction");
                    isConnected = false;
                    break;
                    //  }
                } else {
                    braodcast(msg, false);
                }
            } catch (IOException e) {
            } catch (ClassNotFoundException e) {
            }


        }
    }


    private void braodcast(String msg, boolean toAll) {
        System.out.println("broadcating");
        displayArea.append("\n " + name + " >>> " + msg);
        if (toAll) {
            for (int i1 = 0; i1 < handlers.size(); i1++) {
                MyChatHandler c = handlers.get(i1);
                try {
                    synchronized (c.o) {
                        c.o.writeObject(msg);
                    }
                    c.o.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void braodcastToAllClients(String msg) {
        if (msg.contains("terminate")) {
            for (int i = 0; i < handlers.size(); i++) {
                MyChatHandler c = handlers.get(i);
                try {
                    synchronized (c.o) {
                        c.o.writeObject("closing connection");
                    }
                    c.o.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            for (int j = 0; j < handlers.size(); j++) {
                System.out.println("terminating");
                MyChatHandler c = handlers.get(j);
                try {
                    c.i.close();
                    c.o.close();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            handlers = null;

        } else if (handlers != null) {

            for (int i = 0; i < handlers.size(); i++) {
                MyChatHandler c = handlers.get(i);
                try {
                    synchronized (c.o) {
                        c.o.writeObject(msg);
                    }
                    c.o.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


}
