package mine;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;

/**
 * Created by msi on 5/29/2017.
 */
public class Myclient extends JFrame {

    private JTextField enterField;
    private JTextArea displayArea;
    public ObjectOutputStream output;
    public ObjectInputStream input;
    private String massege = "";
    private String chatServer;
    public Socket client;
//    Myserver thisOne = this;
//    public ActionListener close = new ActionListener() {
//        @Override
//        public void actionPerformed(ActionEvent e) {
//            thisOne.dispose();
//        }
//    };
//

    public Myclient(String host) {
        super("Client");

        chatServer = host;
        Container container = getContentPane();
        enterField = new JTextField();
        enterField.setEnabled(true);
        enterField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sendData(
                        e.getActionCommand()//gives the value of the text field
                );
            }
        });
        container.add(enterField, BorderLayout.NORTH);
        displayArea = new JTextArea();
        container.add(new JScrollPane(displayArea), BorderLayout.CENTER);
        setSize(300, 150);
        setVisible(true);
    }

    public void runClient() {
        try {
            connectToServer();
            getStreams();
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {

                    processConnection();
                }
            });
            t.start();


            //    closeConnection();


        } catch (
                EOFException eofException)

        {
            System.out.println("server terminated connection");
        } catch (
                IOException ioException)

        {
            //   ioException.printStackTrace();
        }

    }


    private void getStreams() throws IOException {
        output = new ObjectOutputStream(client.getOutputStream());
        output.flush();
        input = new ObjectInputStream(client.getInputStream());
        displayArea.append("\n got I/O stream\n");


    }

    private void connectToServer() throws IOException {
        displayArea.setText("attempting to connection\n");
        System.out.println("hi");
        client = new Socket(InetAddress.getByName(chatServer), 5000);
        System.out.println("hi again");
        displayArea.append(" connected to : " + client.getInetAddress().getHostName());

    }

    private void processConnection() {
        System.out.println("starting process");
        enterField.setEnabled(true);
        do {
            try {
                massege = (String) input.readObject();
                displayArea.append("\n" + massege);
                displayArea.setCaretPosition(displayArea.getText().length());
            } catch (EOFException e) {
                displayArea.append("\n connection terminated");
                enterField.setEnabled(false);
                break;
            } catch (ClassNotFoundException e) {
                displayArea.append("\n unkown object recieved");
            } catch (IOException e) {
                // e.printStackTrace();
            }
        } while (!massege.equals("server >>> terminate"));
        System.out.println("process ended");
    }

    private void closeConnection() throws IOException {
        displayArea.append("\n closing connetion");
        output.close();
        input.close();
        client.close();
    }

    private void sendData(String massege) {
        try {
            output.writeObject(massege);
            output.flush();
            displayArea.append("\n client>>>" + massege);
        } catch (IOException e) {
            displayArea.append("\n error writing object");
        }
    }

    public static void main(String[] args) {
        Myclient application;
        //   if (args.length == 0) {
        application = new Myclient("127.0.0.1");
        //application = new Client("192.168.230.1");

        // } else {
        //      application = new Myclient(args[0]);
        // }
        application.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        application.runClient();
    }


}
