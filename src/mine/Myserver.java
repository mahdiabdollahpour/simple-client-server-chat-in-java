package mine;

import javax.swing.*;
import java.awt.*;
import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by msi on 5/29/2017.
 */
public class Myserver extends JFrame {
    private JTextField enterField;
    private JTextArea displayArea;
    private ObjectOutputStream output;
    private ObjectInputStream input;
    private ServerSocket server;
    private Socket connection;
    private int counter = 1;

    public Myserver() {
        super("Server");
        Container container = getContentPane();
        enterField = new JTextField();
        enterField.setEnabled(false);

        enterField.addActionListener(event -> sendData(event.getActionCommand()));
        container.add(enterField, BorderLayout.NORTH);
        displayArea = new JTextArea();
        container.add(new JScrollPane(displayArea), BorderLayout.CENTER);
        setSize(300, 150);
        setVisible(true);

    }

    public void runServer() {
        try {
            server = new ServerSocket(5000, 100);
            while (true) {
                waitForConnection();
            }
        } catch (EOFException eofException) {
            System.out.println("Client terminated connection");
        } catch (IOException ioException) {
          //  ioException.printStackTrace();
        }
    }

    private void waitForConnection() throws IOException {
        displayArea.append("\n Waiting for (other) connection\n");
        connection = server.accept();
        enterField.setEnabled(true);
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                MyChatHandler c = new MyChatHandler(connection, displayArea, "Client" + String.valueOf(counter++));

            }
        });
        t.start();

        displayArea.append("Connection " + counter + " recevied  from : " +
                connection.getInetAddress().getHostName());
    }

    private void sendData(String message) {
        MyChatHandler.braodcastToAllClients("SERVER >>> " + message);
        displayArea.append("\nSERVER >>> " + message);
        if (message.equals("terminate")) {
            enterField.setEnabled(false);
        }
    }

    public static void main(String args[]) {
        Myserver application = new Myserver();

        application.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        application.runServer();

    }


}
